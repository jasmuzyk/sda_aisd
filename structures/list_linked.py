from list_basic import Node


class LinkedList(object):
    def __init__(self):
        self.head = None  # self.head jest Nodem
        self.tail = None  # ma wskazywać na ostatni element na LinkedLiście
        self.counter = 0

    def add_node_to_front(self, liczba):
        new_node = Node(liczba)
        self.counter += 1
        if self.head is None:  # jeśli lista jest pusta
            self.head = new_node
            self.tail = new_node
        else:
            new_node.set_next_node(self.head)
            self.head = new_node

    def print_list(self):
        self.head.print_all_list()

    def check_if_element(self, searched_value):
        '''Jeśli taka wartość jest w liście zwraca True, a jeśli nie to false'''
        temporary_node = self.head
        while temporary_node is not None:
            if temporary_node.val == searched_value:
                return True

            temporary_node = temporary_node.next
        return False

    def add_node_to_end(self, value):
        new_node = Node(value)
        self.counter += 1
        if self.head is None:  # jeśli lista jest pusta
            self.head = new_node
            self.tail = new_node
        else:
            self.tail.next = new_node
            self.tail = new_node

    def check_how_many(self):
        counter = 0
        temporary_node = self.head

        while temporary_node is not None:
            temporary_node = temporary_node.next
            counter += 1

        return counter

    def remove_last_element(self):
        '''Usuwa ostatni element'''
        temporary_node = self.head  # ustawiamy temp_noda na początek
        self.counter -= 1

        while temporary_node.next.next is not None:  # przewijamy do przedostatniego elementu
            temporary_node = temporary_node.next

        self.tail = temporary_node  # update końca listy - ustawiam next w przedostatnim elemencie na Node
        temporary_node.next = None  # i ustawiamy taila na przedostatni element


if __name__ == "__main__":
    nasza_lista = LinkedList()
    nasza_lista.add_node_to_front(24)
    nasza_lista.add_node_to_front(13)
    nasza_lista.add_node_to_front(5)
    nasza_lista.add_node_to_end(7)
    nasza_lista.add_node_to_end(2)
    nasza_lista.remove_last_element()
    nasza_lista.print_list()
